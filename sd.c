#define _GNU_SOURCE
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

#define SSL_ERR (ERR_reason_error_string(ERR_get_error()))
struct url {
	int secure;
	char *hostname;
	char *path;
	char *port;
};

int ssl_verify = 1; /* verify peer if using secure connection */

static void parse_url(char *s, struct url *url)
{
	char *p;

	url->secure = 1;
	url->port = "443";
	url->path = "";
	if ((p = strstr(s, "://"))) {
		if (!strncmp(s, "http://", 7)) {
			url->secure = 0;
			url->port = "80";
		} else if (strncmp(s, "https://", 8))
			errx(1, "protocol not supported: '%s'", s);
		s = p + 3;
	}

	if (!url->hostname || *s != '/') /* relative redirect? */
		url->hostname = s;

	if ((p = strchr(s, ':'))) {
		url->port = s = p + 1;
		*p = '\0';
	}

	if ((p = strchr(s, '/'))) {
		url->path = p + 1;
		*p = '\0';
	}
}

static BIO *server_connect(struct url *url)
{
	static SSL_CTX *ctx = NULL;
	BIO *bio;
	SSL *ssl;

	if (!ctx) {
		if ((ctx = SSL_CTX_new(TLS_client_method())) == NULL)
			goto fail;

		if (SSL_CTX_set_default_verify_paths(ctx) == 0)
			goto fail;

		SSL_CTX_set_verify(ctx, ssl_verify ? SSL_VERIFY_PEER : SSL_VERIFY_NONE, NULL);
	}

	if (url->secure) {
		if ((bio = BIO_new_ssl_connect(ctx)) == NULL)
			goto fail;
		BIO_get_ssl(bio, &ssl);
		SSL_set_tlsext_host_name(ssl, url->hostname);
		if (ssl_verify && !SSL_set1_host(ssl, url->hostname))
			goto fail;
	} else
		if ((bio = BIO_new(BIO_s_connect())) == NULL)
			goto fail;

	BIO_set_conn_hostname(bio, url->hostname);
	BIO_set_conn_port(bio, url->port);
	if (BIO_do_connect(bio) <= 0)
		goto fail;

	fprintf(stderr, "connected to '%s://%s:%s'\n",
			url->secure ? "https" : "http", url->hostname, url->port);
	return bio;
fail:
	errx(1, "cannot connect to '%s://%s:%s': %s (%s)", 
			url->secure ? "https" : "http", url->hostname, url->port, SSL_ERR,
			X509_verify_cert_error_string(SSL_get_verify_result(ssl)));
}

static char* http_header(char *s, char *name)
{
	char *p, *val;

	while ((s = strstr(s, "\r\n")) && (p = strstr(s, ": "))) {
		s += 2;
		if (strlen(name) != (size_t)(p - s) || strncasecmp(s, name, p - s))
			continue;
		val = p + 2;
		if ((p = strstr(val, "\r\n")))
			*p = '\0';
		return val;
	}
	return NULL;
}

static void http_request(BIO *bio, struct url *url)
{
	char buf[4096];

	snprintf(buf, sizeof(buf), "GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n",
			url->path, url->hostname);
	if (BIO_puts(bio, buf) <= 0)
		errx(1, "cannot send request: %s", SSL_ERR);
}

static char *http_receive(BIO *bio, int fd)
{
	int sz;
	char buf[16384], *p, *val;
	long long total = 0, content_len = 0;

	while ((sz = BIO_read(bio, buf + total, sizeof(buf) - total)) > 0)
		total += sz;

	if (!(p = memmem(buf, total, "\r\n\r\n", 4)))
		errx(1, "HTTP header is too big");

	*p = '\0';
	p += 4;
	if ((val = http_header(buf, "location")))
		return val;
	if ((val = http_header(buf, "content-length")))
		content_len = atoll(val);

	sz = buf + total - p;
	total = 0;
	memmove(buf, p, sz);
	do {
		if (write(fd, buf, sz) != sz)
			err(1, "write");
		total += sz;
		fprintf(stderr, "\r%lluK/%lluK", total >> 10, content_len >> 10);
		fflush(stderr);
	} while ((sz = BIO_read(bio, buf, sizeof(buf))) > 0);
	write(2, "\n", 1);
	return NULL;
}

int main(int argc, char **argv)
{
	BIO *bio;
	int i, fd = 1;
	struct url url = {0};
	char *filename = "default", *s = NULL;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-')
			s = argv[i];
		else if (!strcmp(argv[i], "-o"))
			filename = argv[++i];
		else if (!strcmp(argv[i], "-v"))
			ssl_verify = 0;
	}

	if (!s || !filename)
		errx(1, "usage: %s [-o file] [-v] url", argv[0]);

	if (strcmp(filename, "-") && (fd = creat(filename, 0644)) < 0)
		err(1, "open '%s'", filename);

	while (s) {
		parse_url(s, &url);
		bio = server_connect(&url);
		http_request(bio, &url);
		s = http_receive(bio, fd);
		BIO_free_all(bio);
	}
	close(fd);
}
