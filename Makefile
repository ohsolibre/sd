CC = cc
CFLAGS = -Wall -Wextra -Os
LDFLAGS = -s -lssl -lcrypto

sd: sd.c
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)
clean:
	rm -f sd
